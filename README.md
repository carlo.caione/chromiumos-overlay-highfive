# HighFive - CromiumOS integration
This guide describe how to compile and install a new image of ChromiumOS and run it on a NUC hardware. It will also describe the general steps required to introduce a new board overlay and customizations such as new packages, patches, new services, etc...

At the end of each section a set of references is introduced to expand and possibly clarify the topic.
## Source code checkout
### Preliminary setup
Some features of Chromium use Google APIs that are also required to allow user login on the NUC so we need to acquire the keys before compiling a new image. Even though this is not strictly required, it is a straightforward procedure that can be done online.

1. Go to [https://cloud.google.com/console](https://cloud.google.com/console)
2. Click on **Select a project** and then on **New project**
3. Give the project a proper name, i.e. *HighFive* and then click on **Create**
4. Click on **APIs and Services** in the sidebar and then on **Credentials**
5. Click on the **Create credentials** button and select the **OAuth client ID** item
6. Click on **Configure consent screen**
7. Fill in the **Application name**, i.e. *HighFive app* and click on **Save** at the bottom
8. In the **Application type** section check the **Other** option and give it a name in the **Name** text box and then click **Create**
9. In the pop-up window that appears you'll see a **client ID** and a **client secret** string, copy and paste those in a text file then click **OK** to dismiss it
10. Click the **Create credentials** button again on the same page
11. In the pop-over window that shows up click the **API key** button
12. A pop-over should show up giving you the **API key**. Copy and paste it in a text file to save it as well

Now that we have the **client ID**, the **client secret** and the **API key** we can copy those in our dev box.
1. In your home directory create a file called `.googleapikeys`
2. Copy the following content in the file filling in your values:
```
'google_api_key': '$your_api_key',
'google_default_client_id': '$your_client_id',
'google_default_client_secret': '$your_client_secret',    
```
This file will be automatically included when creating the `chroot`.

***NOTE***: to be able to perform some specific actions are use some features it is needed sometimes to enable specific APIs that we discover using the Library reachable at https://console.cloud.google.com/apis/library. See the first link in the reference for more information. 

Finally create a new directory where all the source code is going to live before checking out all the needed code:
```
$ mkdir -p ${HOME}/highfive
```
##### References for the section
- https://www.chromium.org/developers/how-tos/api-keys
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/building-chromium-browser/chrome-api-keys-in-the-chroot

### Checking out ChromiumOS
ChromiumOS uses [repo](https://code.google.com/p/git-repo/) to sync down source code. `repo` is a wrapper for  `git` that helps deal with a large number of `git` repositories. `repo` is released as part of the `depot_tools` so we need to checkout those first:
1. Clone the tools in a proper sub-directory:
```
$ cd ${HOME}/highfive
$ git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
```
2. Make the tools accessible exporting the path:
```
$ export PATH=${PATH}:${HOME}/highfive/depot_tools
```
Before actually checking out the ChromiumOS code we need to decide which release we are targeting. To check which are the official releases we can check the tagged releases at https://chromium.googlesource.com/chromiumos/manifest.git/+refs, where a tagged release is named `release-RXX-YYYY.B`.

Let's say that we want to target the latest `R73` (with tag `release-R73-11647.B`) release, in this case we can checkout the code with:
```
$ mkdir -p ${HOME}/highfive/cros/
$ cd ${HOME}/highfive/cros/
$ repo init \
  -u https://chromium.googlesource.com/chromiumos/manifest.git \
  -b release-R73-11647.B \
  --repo-url https://chromium.googlesource.com/external/repo.git
$ repo sync -j8
```
Adjust the `-jX` parameter according to the number of cores available on the dev machine.
If we want to work on the development branch (not recommended) we can omit the `-b` parameter.
##### References for the section
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Get-the-Source
- https://sites.google.com/a/chromium.org/dev/chromium-os/quick-start-guide
- https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html

### Checking out Chromium browser
Since we want to patch the Chromium browser embedded into ChromiumOS we need also to checkout its source code, otherwise the official release will be used instead. If we don't set up a local Chromium checkout, the ChromiumOS build system does not build Chromium at all, it just downloads a pre-built Chromium image from Google servers during `./build_package` step.

Before checking out the source code for the browser we need to check again which version we want to use. In general the guideline here is to use the same version as it would have been used by the chosen ChromiumOS release.

To get the Chromium version number:
```
$ cd ${HOME}/highfive/
$ ls -m cros/src/third_party/chromiumos-overlay/chromeos-base/chromeos-chrome/*.ebuild
cros/src/third_party/chromiumos-overlay/chromeos-base/chromeos-chrome/chromeos-chrome-73.0.3683.0.ebuild                                                     
cros/src/third_party/chromiumos-overlay/chromeos-base/chromeos-chrome/chromeos-chrome-73.0.3683.77_rc-r1.ebuild
cros/src/third_party/chromiumos-overlay/chromeos-base/chromeos-chrome/chromeos-chrome-9999.ebuild
```
So in this case the Chromium version we are interested in is `73.0.3683.77` but checking out the Chromium source code is a multi-step process:
1. Create a directory to host the Chromium sources and fetch the development branch:
```
$ mkdir -p ${HOME}/highfive/chromium/
$ cd ${HOME}/highfive/chromium/
$ fetch --nohooks chromium
```
2. After it has finished we have to modify the hidden `.gclient` file in the current directory adding the following line (leave intact the rest of the file):
```
...
target_os = ['chromeos']
```
3. We can now sync the repository downloading all the needed third-party packages:
```
$ gclient sync --with_branch_heads --with_tags
```
4. Only at this point we can fetch the tags and switch to the desired version (creating a new `branch_73.0.3683.77` branch):
```
$ cd src
$ git fetch
$ git checkout -b branch_73.0.3683.77 73.0.3683.77
$ gclient sync --with_branch_heads --with_tags
```
##### References for the section
- https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md#Get-the-code
- https://www.chromium.org/developers/how-tos/get-the-code/working-with-release-branches
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Making-changes-to-the-Chromium-web-browser-on-Chromium-OS
- https://chromium.googlesource.com/chromiumos/docs/+/master/simple_chrome_workflow.md
- https://chromium.googlesource.com/chromium/src/+/HEAD/docs/chromeos_build_instructions.md
- https://www.chromium.org/developers/version-numbers

### Integrate HighFive overlay
All the changes specific to the HighFive hardware are embedded into an overlay layer. This layer is used to customize the packages selection, the optimizations used for the image, to add hardware specific patches, etc...

To have the overlay correctly checked out and recognized by the build system:
1. Checkout the overlay in the correct directory:
```
$ cd ${HOME}/highfive/cros/src/overlays/
$ git clone git@gitlab.com:carlo.caione/chromiumos-overlay-highfive.git overlay-highfive
```
2. Modify the `src/third_party/chromiumos-overlay/eclass/cros-board.eclass` file adding the `highfive` layer / board to `ALL_BOARDS`:
```
ALL_BOARD=(
	...
	highfive
	...
)
```
We are now ready to build a new image.
##### References for the section
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide/private-boards
- https://www.chromium.org/chromium-os/external-bsp-hosting

## Building and booting a new image
With all the pieces in place (ChromiumOS and Chromium sources and highfive overlay) we are ready to generate the first image. As starting point we want to generate a test image including several tools useful during development.

To create a test image:
1. Make available `depot_tools` again (if needed):
```
$ export PATH=${PATH}:${HOME}/highfive/depot_tools
```
2. Enter the chroot passing the location of the Chromium sources:
```
$ cd ${HOME}/highfive/cros/
$ cros_sdk --chrome_root=${HOME}/highfive/chromium/ 
```
3. Inside the chroot we can setup the board  with:
```
(chroot) $ export BOARD=highfive
(chroot) $ ./setup_board --board=${BOARD}
```
4. At this point we must instruct the build system to compile Chromium from our sources:
```
(chroot) $ cros_workon --board=${BOARD} start chromeos-chrome
```
5. We can now compile the packages and assemble the image:
```
(chroot) $ ./build_packages --board=${BOARD}
(chroot) $ ./build_image --board=${BOARD} --noenable_rootfs_verification test
```
The result image will be `${HOME}/highfive/cros/src/build/images/highfive/latest/chromiumos_test_image.bin`
##### References for the section
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Building-Chromium-OS
- https://sites.google.com/a/chromium.org/dev/chromium-os/quick-start-guide

## Flashing the image on USB / HD and first boot
### Writing the image to USB flash drive
The easiest / quickest way to test the image is to boot from USB flash disk and install on HD if we are satisfied with the resulting image. Before attempting to write the image on the USB please be sure to disable the auto-mounting of USB devices.

We can flash the latest image on USB from inside the chroot with:
```
(chroot) $ cros flash usb:// ${BOARD}/latest
```
Otherwise (not recommended) we can flash the image from outside the chroot with:
```
$ dd if=${HOME}/highfive/cros/src/build/images/highfive/latest/chromiumos_test_image.bin \
  of=/dev/sdX bs=1M
```
where `sdX` is the device file of the USB flash disk.

We can at this point leave and unmount the chroot with:
```
$ cros_sdk --unmount
```
### First boot and HD installation
Even though not strictly required it is recommended to not boot in legacy mode but boot using UEFI instead. The legacy mode can be disabled in the BIOS settings.
Before powering the NUC on, insert the USB flash drive into the NUC and press `F10` during POST to reach the boot menu and from here select the USB device (should be the latest entry in the list).

After the first setup the system is already fully usable and ready.

To install the image on the HD instead of keep booting from USB we need to access the command prompt on ChromiumOS, we can do that in two different ways:
1. After your computer has booted to the ChromiumOS login screen (or even after the user login), press `[ Ctrl ] [ Alt ] [ F2 ]` to get a text-based login prompt
2. Connect to the machine using SSH

In both cases since we are using a test image the root password is already set to `test0000`.

After we reach the command prompt we can install the image on the HD with:
```
$ /usr/sbin/chromeos-install --dst /dev/nvme0n1
```
This of course is a dangerous / destructive operation and it will wipe the hard disk clean.
##### References for the section
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Installing-Chromium-OS-on-your-Device
- https://sites.google.com/a/chromium.org/dev/chromium-os/quick-start-guide
- https://sites.google.com/a/chromium.org/dev/chromium-os/build/cros-flash
- https://www.chromium.org/chromium-os/chromiumos-design-docs/disk-format

## `overlay-highfive` aka how to customize a ChromiumOS image
### `overlay-highfive` general structure
An overlay is the set of configuration files that define how the build system will build a Chromium OS disk image. Configurations will be different depending on the board and architecture type, and what files a developer wants installed on a given disk image.

The overlay for the HighFive hardware is checked out at `src/overlays/overlay-highfive/` in the ChromiumOS sources and it is composed by three parts:
1. A set of standard files required by the build system to correctly integrate the overlay layer containing among other things the `USE` flags and the compiler optimizations:
```
overlay-highfive/  
|-- metadata/  
|   `-- layout.conf
|-- profiles/base/
|   |-- make.defaults 
|   `-- parent  
`-- toolchain.conf
```
2. The BSP files required for adding any specific drivers and tools to a board. The `chromeos-bsp-highfives-*.ebuild` file is particularly important because it defines the additional hardware-specific set of packages to include in the image:
```
overlay-highfive/
|-- chromeos-base/chromeos-bsp-highfive/
|   |-- chromeos-bsp-highfive-0.0.1.ebuild
|   `-- chromeos-bsp-highfive-0.0.1-r1.ebuild
|-- virtual/chrome-bsp/
    `-- chromeos-bsp-2.ebuild
```
3. A set of skeleton apps to show how to correctly integrate new packages / services into the image:
```
overlay-highfive/
|-- chromeos-base/highfive-daemon/
|   |-- highfive-daemon-0.0.1-r2.ebuild
|   |-- highfive-daemon-0.0.1.ebuild
|   `-- files/
|       `-- highfive-daemon.conf
|-- app-misc/hello-world/
    |-- Manifest
    |-- metadata.xml
    `--hello-world-2.0.ebuild
```
##### References for the section
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide/private-boards
- https://www.chromium.org/chromium-os/external-bsp-hosting

### How to add a new package / application
ChromiumOS uses Gentoo's [portage](https://wiki.gentoo.org/wiki/Portage) (aka `emerge`) as the package manager. This means that when we want to add a new package into the ChromiumOS image we need to write an `ebuild`, that is the recipe to build a package. An `ebuild` file is written in shell script, it should be named _project-version.ebuild_ and it should live in a directory under an appropriate category.

In this section we are not going to detail how to write an `ebuild` file since online there are plenty of tutorials on how to do that (see the references at the end of this section) but we are going to outline how to integrate the new package into ChromiumOS using the board overlay.

In the `overlay-highfive` a new `hello-world` sample application is introduced. The `ebuild` file takes care of downloading the sources, compiling them and install the output binaries into the highfive image as part of the image generation process.

When adding a new package to the image using the overlay layer we need:
1. Create the `ebuild` file for the new package making sure to put the file under an appropriate category (in our case we put the `hello-world` application under `app-misc/hello-world`)
2. If the sources are not hosted on public google mirrors (i.e. the `SRC_URI` in the `ebuild` file is not a google server) we need to specify `nomirror` in the `ebuild` file:
```
SRC_URI="..."
...
RESTRICT="nomirror
```
3. A `Manifest` file should be created in the same location (using again the `hello-world` app as example):
```
$ ebuild hello-world-2.0.ebuild manifest
```
4. A `metadata.xml` file should be present in the directory as well, just copy and modify the file shipped as example
5. To have the package correctly embedded in the image we need to add the `ebuild` as a dependency of our BSP. This is done modifying the BSP `ebuild` file (in our case `chromeos-bsp-highfive-0.0.1.ebuild`) adding a new run-time dependency on our app:
```
...
RDEPEND="...
	 app-misc/hello-world
	 ...
"
...
```

When a new image for the board `highfive` will be created, the `hello-world` application will be downloaded, compiled and automatically installed.
##### References for the section
- `man 5 ebuild`
- https://wiki.gentoo.org/wiki/Portage
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/add-a-new-package
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/portage-build-faq
- https://wiki.gentoo.org/wiki/Basic_guide_to_write_Gentoo_Ebuilds
- https://devmanual.gentoo.org/quickstart/
- https://gitweb.gentoo.org/repo/gentoo.git/tree/skel.ebuild

### How to add a new upstart daemon / boot service
Adding a new daemon is not much different than adding a new C application. We are basically going to add a new job configuration file in place. Also in this section we are not going to detail how upstart works or how to write a configuration file (see the related references for that) but how to integrate that into ChromiumOS.

In the `overlay-highfive` a sample daemon called `highfive-daemon` is present to be used as skeleton. It just starts a new `netcat` instance listening on the port 5555.

When integrating a new upstart daemon in ChromiumOS we need to pay attention to some aspects:
1. The job must depend on `system-services`, thus in the job configuration file we have:
```
...
start on started system-services
...
```
2. The `ebuild` file for the daemon must set a run-time dependency on `chromeos-init` and on the application that we intend to start, `netcat` in our example:
```
...
RDEPEND="...
	 chromeos-base/chromeos-init
	 net-analyzer/netcat
	 ...
"
```
3. The `ebuild` file must take care of copying the job configuration file to the correct location:
```
...
scr_install() {
	insinto /etc/init
	doins "${FILESDIR}"/*.conf
}
...
```

The daemon will be started at boot as a regular boot service.
##### References for the section
- http://upstart.ubuntu.com/cookbook/
- https://www.chromium.org/chromium-os/chromiumos-design-docs/boot-design

### How to patch the Chromium browser
We are compiling Chromium using the sources passed to the chroot using the `--chrome_root` parameter. This means that we can work on Chromium simply patching the source code, compiling the package again and creating a new image.

In steps:
1. Patch / change the source code working directly on the code checked out at `${HOME}/highfive/chromium/`
2. If not already inside, enter again into the chroot passing the correct location for the browser sources:
```
$ cros_sdk --chrome_root=${HOME}/highfive/chromium/ 
```
3. Export the board again if needed:
```
(chroot) $ export BOARD=highfive
```
4. Stop using the release shipped by ChromiumOS. This step should not be needed because we already signaled that we want to compile Chromium from sources when we compiled the image the first time:
```
(chroot) $ cros_workon --board=${BOARD} start chromeos-chrome
```
5. Compile and install again the Chromium browser:
```
(chroot) $ cros_workon_make --board=${BOARD} chromeos-chrome
(chroot) $ cros_workon_make --board=${BOARD} chromeos-chrome --install
```
6. Regenerate the ChromiumOS image:
```
(chroot) $ ./build_image --board=${BOARD} --noenable_rootfs_verification test
```

The image can now be flashed again on the device.
##### References for the section
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Making-changes-to-the-Chromium-web-browser-on-Chromium-OS
- https://chromium.googlesource.com/chromiumos/docs/+/master/simple_chrome_workflow.md
- https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md
- https://chromium.googlesource.com/chromium/src/+/HEAD/docs/chromeos_build_instructions.md

### How to patch a generic package
During development sometimes we need to patch or work on a generic package shipped in the image. How this is done depends on what kind of package we are going to work on: this can be either an existing packages whose source code is not checked into a ChromiumOS git repository (e.g. it comes from portage usually under `src/third_party/portage-stable`) or it is a `cros_workon`-able package usually checked out under `src/third_party/chromiumos-overlay/chromeos-base/`.

`cros_workon`-able packages are packages fetch usually from git.chromium.org. Those ebuilds invariably inherit from the `cros-workon` eclass that adds some additional functionality to the `ebuild`.

#### Portage packages (non `cros_workon`-able packages)
Patching a portage package is not a daily occurrence and usually it should be never really needed.

In this case we just need to copy the portage package in our overlay, add the patches and bump the `ebuild` version so that our version has higher priority with respect to the version shipped in the portage tree.

More in detail:
1. Copy the package from the portage tree (usually `src/third_party/portage-stable`) in the overlay directory keeping the directory structure defining the category of the application
2. Place the patch in the `files` sub-directory of the directory that contains the `ebuild` file
3. In the `prepare()` section of the `ebuild` (create one if it doesn't exist), add an `epatch` line:
```
...
epatch "${FILESDIR}"/${P}-my-patch.patch
...
```
4. Bump the revision number in the name of the `ebuild` file (or `symlink`) so the build system picks up the change
5. Rebuild packages and image

##### References for the section
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#making-changes-to-non_cros_workon_able-packages
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/portage-build-faq#TOC-How-do-I-modify-a-portage-package-
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/portage-build-faq#TOC-How-do-I-uprev-an-ebuild-

#### ChromiumOS packages (`cros_workon`-able packages)
Working on this kind of packages is slightly more difficult but also extremely convenient because usually the source code is already available as part of the ChromiumOS checked out code.

For the sake of example we might want to patch the kernel shipped with ChromiumOS. The kernel is a `cros_workon`-able package.

To patch the kernel we need to:
1. Retrieve the name of the package we are interested in patching / working on, in our example the Linux kernel:
```
(chroot) $ export BOARD=highfive
(chroot) $ equery-${BOARD} list '*' | grep kernel
sys-kernel/chromeos-kernel-4_14-4.14.105-r727
sys-kernel/linux-firmware-0.0.1-r226
sys-kernel/linux-headers-4.14-r4
```
2. Mark the package (`sys-kernel/chromeos-kernel` in our case) as active:
```
(chroot) $ cros_workon --board=${BOARD} start sys-kernel/chromeos-kernel-4_14
```
3. Find where the actual sources for the package are located so that we can start hacking on the source code. To do that we are going to look at the third column in the following output:
```
(chroot) $ cros_workon --board=${BOARD} --all info | grep sys-kernel/chromeos-kernel-4_14
sys-kernel/chromeos-kernel-4_14 chromiumos/third_party/kernel src/third_party/kernel/v4.14
```
4. Start hacking on the source code located at `src/third_party/kernel/v4.14`
5. Compile and install the package with our changes:
```
(chroot) $ cros_workon_make --board=${BOARD} sys-kernel/chromeos-kernel-4_14
(chroot) $ cros_workon_make --board=${BOARD} sys-kernel/chromeos-kernel-4_14 --install
```
6. Repack the image again with:
```
(chroot) $ ./build_image --board=${BOARD} --noenable_rootfs_verification test
```
Being this a package shipped with ChromiumOS all the changes are supposed to be sent upstream for review and inclusion in the official release. If we want to carry the changes locally it is always possible to create a local BSP package as described in the previous section.

After we are done with the changes the most important thing to do is to tell `cros_workon` that we are done by running the following command:
```
(chroot) $ cros_workon --board=highfive stop sys-kernel/chromeos-kernel-4_14
```
This command tells `cros_workon` to stop forcing a build from source every time, but of course our local changes are not going to be included in the package anymore.
##### References for the section
- https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Making-changes-to-packages-whose-source-code-is-checked-into-Chromium-OS-git-repositories

## OTA updates
### Dev Server
##### Starting a Dev Server on the development machine
Every time a new Chromium OS build is created the URL of the Dev Server (corresponding to your development machine) is put into `/etc/lsb-release` and this is used as Dev Server URL defining where to get the new images. So if nothing is specified at build time, the development machine is meant to be used as Dev Server as well.

Let's assume we have generated the initial ChromiumOS image as detailed in the previous sections and we have correctly flashed this image on the machine. At a certain point for some reason we want to release a new image to be delivered as OTA upgrade. To do so we follow again the same steps and we generate the new image with the usual commands up to `./build_image --noenable_rootfs_verification ...`.

This time instead of flashing the new image we start the Dev Server with:
```
(chroot) $ start_devserver
```
At this point the Dev Server is started and keep listening on the `8080` port, ready to deliver the new image when requested by ChromiumOS.

##### Standalone Dev Server
On the contrary sometimes it is reasonable to think that your development machine is different from your update server.  In this case to change the URL from which the updates are going to be fetched we have two options:
1. change the update server URL directly modifying `/mnt/stateful_partition/etc/lsb-release` before flashing the initial image (not recommended).
2. specify the server IP and port (i.e. `8888`) at build time with:
```
(chroot) $ export CHROMEOS_VERSION_DEVSERVER="http://YourIPAddress:8888" 
(chroot) $ export CHROMEOS_VERSION_AUSERVER="http://YourIPAddress:8888/update"
(chroot) $ ./build_packages --board={BOARD}
(chroot) $./build_image --board={BOARD} --noenable_rootfs_verification test
...
```
In the latter case we need to have a standalone Dev Server running on `YourIPAddress:8888` from which the updates are going to be fetched.

The easiest way to have a standalone Dev Server running on a remote server is just to start it into the usual chroot feeding it the correct image to push using OTA. Practically this means following all the steps detailed in the previous sections without actually creating any image (that will be created separately on the development machine):
```
$ repo init ...
$ repo sync ...
$ cros_sdk
(chroot) $ start_devserver --image=${new_OTA_image}
``` 
where `${new_OTA_image}` is the new image (i.e. `chromiumos_test_image.bin`) to be pushed OTA.
### Start the OTA upgrade
On the machine we can fetch the new image using the GUI going to `Settings -> About Chromium OS -> Check for updates` or directly using the CLI with:
```
$ update_engine_client --update
```
This should start the image upgrade and a reboot is requested at the end of the process.

### Rollback
To rollback to the previous version we can use again the update client with:
```
$ update_engine_client --rollback --nopowerwash
```

### Using Dev Server to build and install single packages
​Sometimes during development we want to make a change to a single package and install it on the target machine without recompiling the whole image. To do so we can use the `gmerge` tool. `gmerge` runs on the target machine and contacts the Dev Server to build and deliver packages.

For example to recompile Chromium on the Dev Server and installing it on the test machine we can use:
```
$ gmerge chromeos-base/chromeos-chrome
```
##### References for the section
- https://www.chromium.org/chromium-os/how-tos-and-troubleshooting/using-the-dev-server

