EAPI=5

DESCRIPTION="The highfive meta package to pull in driver/tool dependencies"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="-* amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	 app-misc/hello-world
	 chromeos-base/highfive-daemon
"
