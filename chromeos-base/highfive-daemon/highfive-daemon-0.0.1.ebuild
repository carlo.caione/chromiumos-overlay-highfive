# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

DESCRIPTION="Install a sample upstart job"
HOMEPAGE="https://highfive.com/"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="-* amd64"
IUSE=""

S=${WORKDIR}

RDEPEND="
	chromeos-base/chromeos-init
	net-analyzer/netcat
"

src_install() {
	insinto /etc/init
	doins "${FILESDIR}"/*.conf
}
