# Copyright 2019 BayLibre. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="Chrome OS BSP virtual package"
HOMEPAGE="http://src.chromium.org"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64"

RDEPEND="chromeos-base/chromeos-bsp-highfive
	 sys-kernel/linux-firmware
"
